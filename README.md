# A5---BANCO-DE-DADOS
Projeto avaliando nota final

Link para a aplicação funcionando, publicada da Internet:
http://casa.apolonioserafim.com.br:8081/

**Tecnologias utilizadas:**
- Bootstrap ( HTML, CSS, JS )
- Jquery
- PHP
- Composer para instalação da lib necessária para funcionamento do driver do bd com o php
- MONGODB

A modelagem de dados escolhida foi a baseada em documentos, por ser um modelo de dados mais flexivel e por não ter necessariamente um tipo de estrutura pré-definida, o que pode proporcionar uma melhor escalabilidade do sistema.

	<?php

define('DBNAME', 'baseproj');
define('DBHOST', 'localhost');

define('CONTROL', dirname(__FILE__).'/controller');
define('MODEL', dirname(__FILE__).'/model');
define('VIEW', dirname(__FILE__).'/view');
define('VENDOR',dirname(__FILE__).'/vendor');

define('ATIVIDADECONTROL',CONTROL.'/atividade');
define('LISTACONTROL',CONTROL.'/lista');
define('SISTEMA',CONTROL.'/sistema');
define('USUARIOCONTROL',CONTROL.'/usuario');


<?php
session_start();
require_once dirname(__FILE__).'/../config.php';
require_once dirname(__FILE__).'/atividadeControl.php';
require_once USUARIOCONTROL.'/usuarioControl.php';

$user = unserialize($_SESSION["autenticado"]);
$control = new atividadeControl();

date_default_timezone_set('America/Sao_Paulo');
$dataCriacao = array(
  'dia' => date('d'),
  'mes' => date('m'),
  'ano' => date('Y')
  );
$dataFinal = explode('-',$_POST['dataFinal']);
$dataFinal = array(
    'dia' => $dataFinal[2],
    'mes' => $dataFinal[1],
    'ano' => $dataFinal[0]
);
/*
	Modelagem do objeto Atividade
	Modelagem do documento a ser enviado ao DB
	{
		_id:'objID',
		titulo:string,
		descricao:string,
		finalizado:bool,
		usuario: 'objID',
		lista: 'objID',
		dataCriacao: array( dia = int,
							mes = int,
							ano = int)
		dataFinal: array( dia = int,
							mes = int,
							ano = int)
	}
	*/

$atividade = new Atividade(
    null,
    $_POST['titulo'],
    $_POST['desc'],
    false,
    $user->getId(),
    $_GET['id'],
    $dataCriacao,
    $dataFinal
);


$control->adicionarAtvd($atividade);


header('Location: ../../view/producao.php?id='.$_GET['id'].'&tA='.$_POST['titulo']);
<?php
include_once dirname(__FILE__).'/../config.php';
include_once SISTEMA.'/conexao.php';
require_once MODEL.'/listaModel.php';
require_once MODEL.'/atividadeModel.php';


Class atividadeControl{

    /*
	Modelagem do objeto Atividade
	Modelagem do documento a ser enviado ao DB
	{
		_id:'objID',
		titulo:string,
		descricao:string,
		finalizado:bool,
		usuario: 'objID',
		lista: 'objID',
		dataCriacao: array( dia = int,
							mes = int,
							ano = int)
		dataFinal: array( dia = int,
							mes = int,
							ano = int)
	}
	*/

    public function adicionarAtvd(Atividade $atividade){
        $colAtividades = abrirConexao('atividades');
        
        $inserirDados = $colAtividades->insertOne(
            [
                'titulo' => $atividade->getTitulo(),
                'descricao' => $atividade->getDescricao(),
                'finalizado' => $atividade->getFinalizado(),
                'usuario' => $atividade->getUsuario(),
                'lista' => $atividade->getLista(),
                'dataCriacao' => $atividade->getDataCriacao(),
                'dataFinal' => $atividade->getDataFinal()
            ]
        );
    }
    public function getAtividades(Lista $lista){
        $colAtividades = abrirConexao('atividades');

        $cursor = $colAtividades->find(
            [
                'lista' => $lista->getId(), 
                'finalizado' => false
            ]
        );
        return $cursor;
    }

    public function getAtividade(Atividade $atividade){
        $colAtvd = abrirConexao('atividades');

        $cursor = $colAtvd->findOne(
            [
                '_id' =>new MongoDB\BSON\ObjectID($atividade->getId())
            
            ]
        );
        $atvdRetorno = new atividade(
                $cursor['_id'],
                $cursor['titulo'],
                $cursor['descricao'],
                $cursor['finalizado'],
                $cursor['usuario'],
                $cursor['lista']
            );
        return $atvdRetorno;
    }

    public function delAtividade(Atividade $atividade){
        $colAtividades = abrirConexao('atividades');

        $resultado = $colAtividades->deleteOne(
            [
                '_id' => new MongoDB\BSON\ObjectID($atividade->getId())
            ]
        );
        return $resultado;
    
    }
    public function editAtividade(Atividade $atividade){
        $colAtividades = abrirConexao('atividades');
        $resultado = $colAtividades->updateOne(
            [
                '_id' => new MongoDB\BSON\ObjectID($atividade->getId()) 
            ],
            [
                '$set' => [
                    'titulo' => $atividade->getTitulo(), 
                    'descricao' => $atividade->getDescricao(),
                    'dataFinal' => $atividade->getDataFinal()
                    ]
            ]
        );
        return $resultado;
    }
    public function getFinalizados(Lista $lista){
        $colAtividades = abrirConexao('atividades');
        $cursor = $colAtividades->find(
            [
                'lista' => $lista->getId(), 
                'finalizado' => true
            ]
        );
        return $cursor;
    }
    public function finalizarAtividade(Atividade $atividade){
        $colAtividades = abrirConexao('atividades');
        $resultado = $colAtividades->updateOne(
            [
                '_id' => new MongoDB\BSON\ObjectID($atividade->getId()) 
            ],
            [
                '$set' => 
                [
                    'finalizado' => true
                ]
            ]
        );
        return $resultado;
    }
}
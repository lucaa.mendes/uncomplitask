<?php 
session_start();
require_once dirname(__FILE__).'/atividadeControl.php';
$user = unserialize($_SESSION["autenticado"]);
$id = $_GET['id'];
$control = new AtividadeControl();
$dataFinal = explode('-',$_POST['dataFinal']);
$dataFinal = array(
    'dia' => $dataFinal[2],
    'mes' => $dataFinal[1],
    'ano' => $dataFinal[0]
);
$atividade = new Atividade($id,$_POST['titulo'],$_POST['descricao'],null,null,null,null,$dataFinal);

$resultado = $control->editAtividade($atividade);
header('location: ../../view/producao.php?modA&id='.$_GET['idL']);


<?php
include_once dirname(__FILE__).'/../config.php';
include_once SISTEMA.'/conexao.php';
require_once MODEL.'/usuarioModel.php';
require_once MODEL.'/listaModel.php';
Class listaControl{
	/*
	Modelagem do documento pra enviar pro DB
	{
		_id: 'objID',
		titulo: string,
		usuario: 'objID'
	}
	*/
    public function adicionarLista(Lista $lista){
		$colListas = abrirConexao('listas');

		$inserirDados = $colListas->insertOne(
			[
				'titulo' => $lista->getTitulo(),
				'usuario' => $lista->getUsuario()
			]
		);
	}
	public function getListas(Usuario $user){
		$colListas = abrirConexao('listas');

		$cursor = $colListas->find(
			[
				'usuario' =>$user->getId()
			]
		);
		return $cursor;
	}
	public function getLista(Lista $lista){
		$colListas = abrirConexao('listas');

		$cursor = $colListas->findOne(
			[
				'_id' =>new MongoDB\BSON\ObjectID($lista->getId())
			
			]
		);
		$listaretorno = new Lista(
				$cursor['_id'],
				$cursor['titulo'],
				$cursor['usuario']
			);
		return $listaretorno;
	}
	public function delLista(Lista $lista){
		$colAtividades = abrirConexao('atividades');
		$colListas = abrirConexao('listas');
		
		$colAtividades->deleteMany(
			[
				'lista' => $lista->getId()
			]
		);

		$resultado = $colListas->deleteOne(
			[
				'_id' =>new MongoDB\BSON\ObjectID($lista->getId())
			]
		);
		return $resultado;

	}
}
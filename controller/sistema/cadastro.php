<?php 
require_once dirname(__FILE__).'/conexao.php';

require_once USUARIOCONTROL.'/usuarioControl.php';

$userControl = new UsuarioControl();
$user = new Usuario(null,$_POST['login'],$_POST['senha'],$_POST['email'],$_POST['nome'],$_POST['sobrenome'],$_POST['idade']);

if($userControl->inserirDados($user)){
	header('Location: ../../index.php');
}else{
	header('Location: ../../view/cadastro.php?error=1');
}
<?php 
require_once dirname(__FILE__).'/conexao.php';
require_once USUARIOCONTROL.'/usuarioControl.php';


$userControl = new UsuarioControl();
$user = new Usuario(null,$_POST['login'],$_POST['senha']);



if($userControl->verificaLogin($user)){
	session_start();
	$user = $userControl->buscarDados($user);
    $_SESSION["autenticado"] = serialize($user);
	header('location: ../../view/producao.php?l');
}else{
	header('location: ../../index.php?e');
}
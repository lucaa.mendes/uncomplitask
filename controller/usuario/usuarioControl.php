<?php 
require_once dirname(__FILE__)."/../config.php";
require_once SISTEMA.'/conexao.php';
require_once MODEL.'/usuarioModel.php';


Class UsuarioControl{
	
	/*

	Modelagem do documento a ser passado para o DB
	{
		_id: 'objID';
		login: string,
		senha: string,
		email: string,
		nome: string,
		sobrenome: string,
		idade: int
	}

	 */
	public function inserirDados(Usuario $user){
		$colUsuarios = abrirConexao('usuarios');
		try{
			$inserirDados = $colUsuarios->insertOne([
				'login' => $user->getLogin(),
				'senha' => $user->getSenha(),
				'email' => $user->getEmail(),
				'nome' => $user->getNome(),
				'sobrenome' => $user->getSobrenome(),
				'idade' => $user->getIdade()
			]);
		}catch(Exception $e){
			echo 'ERRO: ' . $e->getMessage();
		}
		/*debug
		printf("inseriu %d documentos/n", $inserirDados->getInsertedCount());
		var_dump($inserirDados->getInsertedId()); */
	}
	public function verificaLogin(Usuario $user){
		$colUsuarios = abrirConexao('usuarios');
		$usuarioBusca = $this->buscarDados($user);
		try{
			if($user->getSenha() == $usuarioBusca->getSenha()){
				return true;
			}else{
				return false;
			}
		}catch(Exception $e){
			echo 'ERRO: ' . $e->getMessage();
		}
	}
	public function buscarDados(Usuario $user){
		$colUsuarios = abrirConexao('usuarios');
		$uBusca = $colUsuarios->findOne(
			[
				'login' => $user->getLogin()
			]
		);
		$userRetorno = new Usuario(
			$uBusca['_id'],
			$uBusca['login'],
			$uBusca['senha'],
			$uBusca['email'],
			$uBusca['nome'],
			$uBusca['sobrenome'],
			$uBusca['idade']
		);
		return $userRetorno;
	}

}

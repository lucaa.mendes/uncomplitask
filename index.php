<?php 
session_start();
if(isset($_SESSION['autenticado'])){
  header('location: view/producao.php');
}
$login = true; ?>
<!DOCTYPE html>
  <html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>unCompliTask</title>
      <meta charset="utf-8">
      <!--Bootstrap CDN-->
      <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <!--Costumizando estilos-->
      <link rel="stylesheet" href="css/estilo.css">
      <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
  </head>
  <body>

    <header>
        <?php include_once 'view/navbar.php'; ?>
    </header>
    <main>

<div class="login-form">

  <form action="controller/sistema/logar.php" method="post">
    <h2 class="text-center">Entrar</h2>       
    <div class="form-group">
        <input type="text" class="form-control" placeholder="Usuário" required="required" name="login">
    </div>
    <div class="form-group">
        <input type="password" class="form-control" placeholder="Senha" required="required" name="senha">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-dark btn-block">Entrar</button>
    </div>
     
    <div class="clearfix">
        <p class="text-center"><a href="view/cadastro.php">Cadastre-se</a></p>
    </div>
  </form>
</div>

<?php 

$imprime= false;
$tempo = null;
$corpo = null;
$tempo = "agora";
    if(isset($_GET['l'])){
      $imprime = true;
      $corpo = "Até mais, :)";
    }else if(isset($_GET['e'])){
      $imprime = true;
      $corpo = "[ERRO] Algo de errado aconteceu... <br> 
      Será que você escreveu seus dados corretamente?";

    }else{
      $imprime = true;
      $corpo = "Seja bem vindo, faça login ou cadastre-se
                  <br>
                  para acessar o sistema";
    }


     if($imprime){
      echo "<div aria-live='polite' aria-atomic='true' style='position: relative; min-height: 200px; '>
            <!-- Position it -->
            <div style='position: absolute; bottom: 40vh; right: 5vw; min-width: 250px;'>

              <!-- Then put toasts within -->
              <div class='toast ' role='alert' aria-live='assertive' aria-atomic='true' data-autohide='true' data-delay='5000'>
                <div class='toast-header'>
                  <img src='img/favicon.ico' class='rounded mr-2' alt='...' style='max-height: 20px'>
                  <span class='mr-auto'>uncompliTask</span>
                  <small class='text-muted'>".$tempo."</small>
                </div>
                <div class='toast-body'>
                  ".$corpo."
                </div>
              </div>

            </div>
            </div>";
     }
           ?>



    </main>

    <footer>
      

    <?php require_once "view/rodape.php"; ?>
    </footer>

    
          <!--JS-->
          <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
      </script>
      <script src="js/script.js"></script>
      





  </body>

  </html>
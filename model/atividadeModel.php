<?php
Class Atividade{
	/*
	Modelagem do objeto Atividade
	Modelagem do documento a ser enviado ao DB
	{
		_id:'objID',
		titulo:string,
		descricao:string,
		finalizado:bool,
		usuario: 'objID',
		lista: 'objID',
		dataCriacao: array( dia = int,
							mes = int,
							ano = int)
		dataFinal: array( dia = int,
							mes = int,
							ano = int)
	}
	*/
	private $id;
	private $titulo;
	private $descricao;
	private $finalizado;
	private $usuario;
	private $lista;
	private $dataCriacao;
	private $dataFinal;

	function __construct($id = null,$titulo = null,$descricao = null,$finalizado = null,$usuario = null,$lista = null,$dataCriacao = null,$dataFinal = null){
		$this->setId($id);
		$this->setTitulo($titulo);
		$this->setDescricao($descricao);
		$this->setFinalizado($finalizado);
		$this->setUsuario($usuario);
		$this->setLista($lista);
		$this->setDataCriacao($dataCriacao);
		$this->setDataFinal($dataFinal);
	}

	public function getId(){
		return $this->id;
	}
	public function setId($id){
		$this->id = $id;
	}
	public function getTitulo(){
		return $this->titulo;
	}
	public function setTitulo($titulo){
		$this->titulo = $titulo;
	}
	public function getDescricao(){
		return $this->descricao;
	}
	public function setDescricao($descricao){
		$this->descricao = $descricao;
	}
	public function getFinalizado(){
		return $this->finalizado;
	}
	public function setFinalizado($finalizado){
		$this->finalizado = $finalizado;
	}
	public function getUsuario(){
		return $this->usuario;
	}
	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}
	public function getLista(){
		return $this->lista;
	}
	public function setLista($lista){
		$this->lista = $lista;
	}
	public function setDataCriacao($dataCriacao){
		$this->dataCriacao = $dataCriacao;
	}
	public function getDataCriacao(){
		return $this->dataCriacao;
	}
	public function setDataFinal($dataFinal){
		$this->dataFinal = $dataFinal;
	}
	public function getDataFinal(){
		return $this->dataFinal;
	}
}
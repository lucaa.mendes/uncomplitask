<?php 

Class Lista{
	/*
	Modelagem do documento pra enviar pro DB
	{
		_id: 'objID',
		titulo: string,
		usuario: 'objID'
	}
	*/
	private $id;
	private $titulo;
	private $usuario;
	function __construct($id = null,$titulo = null,$usuario = null)
	{
			$this->setId($id);
			$this->setTitulo($titulo);
			$this->setUsuario($usuario);
	}
	public function getId(){
		return $this->id;
	}
	public function setId($id){
		$this->id = $id;
	}
	public function getTitulo(){
		return $this->titulo;
	}
	public function setTitulo($titulo){
		$this->titulo = $titulo;
	}
	public function getUsuario(){
		return $this->usuario;
	}
	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}
}
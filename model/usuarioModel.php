<?php
Class usuario{
	/*
	Modelo do objeto Usuário
	Modelagem do documento a ser passado para o DB
	{
		_id: 'objID';
		login: string,
		senha: string,
		email: string,
		nome: string,
		sobrenome: string,
		idade: int
	}

	*/
	private $id;
	private $login;
	private $senha;
	private $email;
	private $nome;
	private $sobrenome;
	private $idade;
	function __construct($id = null,$login = null,$senha = null,$email = null,$nome = null,$sobrenome = null,$idade = null){
		$this->setId($id);
		$this->setLogin($login);
		$this->setSenha($senha);
		$this->setEmail($email);
		$this->setNome($nome);
		$this->setSobrenome($sobrenome);
		$this->setIdade($idade);
	}
	public function setId($id){
		$this->id = $id;
	}
	public function getId(){
		return $this->id;
	}
	public function setLogin($login){
		$this->login = $login;
	}
	public function getlogin(){
		return $this->login;
	}
	public function setSenha($senha){
		$this->senha = $senha;
	}
	public function getSenha(){
		return $this->senha;
	}
	public function setEmail($email){
		$this->email = $email;
	}
	public function getEmail(){
		return $this->email;
	}
	public function setNome($nome){
		$this->nome = $nome;
	}
	public function getNome(){
		return $this->nome;
	}
	public function setSobrenome($sobrenome){
		$this->sobrenome = $sobrenome;
	}
	public function getSobrenome(){
		return $this->sobrenome;
	}
	public function setIdade($idade){
		$this->idade = $idade;
	}
	public function getIdade(){
		return $this->idade;
	}

}


<?php require_once '../controller/sistema/verificaLogin.php'; 
if(isset($_GET['id']))
{
  $temID = True; 
} else{
  $temID = false;
}
$user = unserialize($_SESSION['autenticado']);
?>

  <!DOCTYPE html>
  <html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>unCompliTask</title>
      <meta charset="utf-8">
      <!--Bootstrap CDN-->
      <link rel="stylesheet" href="../css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <!--Costumizando estilos-->
      <link rel="stylesheet" href="../css/estilo.css">
      <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon" />
  </head>


  <body>
    
     <header>
     <?php require_once 'navbar.php'; ?>
     </header>

    <main>

      <div class="container-fluid ">
        <div class="row">
          <div class="col-4  col-sm-3  col-md-2 coluna-lateral overflow-auto" >
            <div class="conteudoCol text-center">
                <nav class="navbar navbar-light bg-light" id="ind">
                <a class="navbar-brand linkIndice" href="#">Indice</a>
                <nav class="nav nav-pills flex-column">
                  <a class="nav-link linkIndice" href="#inicio">Inicio</a>
                  <nav class="nav nav-pills flex-column">
                    <a class="nav-link ml-3 my-1 linkIndice" href="#inicio">Como utilizar?</a>
                    <a class="nav-link ml-3 my-1 linkIndice" href="#comeco">Como começar?</a>
                  </nav>
                  <a class="nav-link linkIndice" href="#addLista">Listas</a>
                  <nav class="nav nav-pills flex-column">
                    <a class="nav-link ml-3 my-1 linkIndice" href="#addLista">Adicionar Listas</a>
                    <a class="nav-link ml-3 my-1 linkIndice" href="#delLista">Remover Listas</a>
                  </nav>
                  <a class="nav-link linkIndice" href="#addAtv">Atividades</a>
                  <nav class="nav nav-pills flex-column">
                    <a class="nav-link ml-3 my-1 linkIndice" href="#addAtv">Adicionar Atividades</a>
                    <a class="nav-link ml-3 my-1 linkIndice" href="#modAtv">Modificar Atividades</a>
                  </nav>
                </nav>
              </nav>
              
              </div>
          </div>
              <div class="col-8  col-sm-9  col-md-10 principal">
                <div class="conteudoP overflow-auto"  data-spy="scroll" data-target="#ind" data-offset="50">
                      <div id="inicio">
                        <a href="#inicio" class="indice"><h5>#Como utilizar?</h5></a>
                        <p>O sistema do uncompliTask teve como objetivo uma utilização simples e limpa para compreender a questão de execução de atividades</p>
                      </div>

                      <div id="comeco">
                        <a href="#comeco" class="indice"><h5>#Como começar?</h5></a>
                        <p>Após ter feito o login você deve ter se deparado com a página inicial em branco, onde não tinha nenhuma lista selecionada e nenhuma atividade apresentada.</p>
                        <img src="../img/pagInicio.png" alt="Página inicial" style="max-height: 300px;" class="img-thumbnail">
                        <p>A partir desta página que poderão ser administradas as suas listas com as devidas atividades, aqui fica uma imagem de exemplo com o sistema em funcionamento</p>
                        <img src="../img/funcionamento.png" alt="Página inicial em funcionamento" style="max-height: 300px;" class="img-thumbnail">
                      </div>
                      
                      <div id="addLista">
                        <a href="#addLista" class="indice"><h5>#Como criar Listas?</h5></a>
                        <p>Para fazer sua primeira coleção de atividades, basta clicar no botão de + ao lado do nome listas na coluna lateral e assim designar um nome para a lista</p>
                        <div class="imagens">
                        <img src="../img/lista1.png" alt="Adicionar Lista" style="height: 100px;" class="img-thumbnail">
                        <img src="../img/lista2.png" alt="Form de Adicionar Lista" style="max-height: 100px;" class="img-thumbnail">
                        </div>
                        <p>Para fechar o formulario basta clicar novamente no botão +</p>
                        
                      </div>
                      
                      <div id="delLista">
                        <a href="#delLista" class="indice"><h5>#Como remover Listas?</h5></a>
                        <p>Para remover a lista, basta selecionar a lista clicando em seu titulo na coluna lateral que irá liberar o botão para removê-la</p>
                        <img src="../img/delLista.png" alt="Form de Adicionar Lista" style="max-height: 200px;" class="img-thumbnail"><br>
                        Após clicar a lista será removida automaticamente e todas as suas atividades serão excluídas.
                      </div>

                      <div id="addAtv">
                        <a href="#addAtv" class="indice"><h5>#Como criar Atividades?</h5></a>
                        <p>Para inserir atividades em alguma lista, primeiro é necessário selecionar a lista na coluna lateral, selecionando a lista irá liberar o botão para adicionar uma atividade</p>
                        <img src="../img/addAtvd.png" alt="adicionar Atividade" style="max-height: 300px;" class="img-thumbnail">
                        <p>Clicando nela irá abrir uma janela para fazer a inserção dos dados da atividade, titulo e descrição.</p>

                      </div>

                      <div id="modAtv">
                        <a href="#modAtv" id="modAtv" class="indice"><h5>#Como Modificar Atividades?</h5></a>
                        <p>Para acessar a modificação de Atividades, basta clicar no próprio titulo, irá aparecer um formulário semelhante ao que aparece para adicionar atividades, basta inserir as novas informações e salvar</p>
                        <img src="../img/modAtvd.png" alt="Modificar Atividade" style="max-height: 500px;" class="img-thumbnail">
                        <p>Para finalizar basta clicar no botão de CHECK verde no titulo da atividade e para remover no botão CLOSE do lado direito</p>
                        <img src="../img/atividade.png" alt="Modificar Atividade" style="max-height: 500px;" class="img-thumbnail">
                      </div>
                  </div>
                </div>
              </div>
            </div>
    </main>
<footer>
<?php require_once "rodape.php"; ?>
</footer>
      <!--JS-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script src="../js/script.js"></script>
  </body>

  </html>
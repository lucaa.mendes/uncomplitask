<?php
require_once '../controller/atividade/atividadeControl.php';
$control = new AtividadeControl();

if($temID){
  $lista = new Lista($_GET['id']);
  $atividades = $control->getAtividades($lista);

  foreach($atividades as $atividade){
    $dataCriacao = $atividade['dataCriacao'];
    $dataCriacao = $dataCriacao['dia'].'/'.$dataCriacao['mes'].'/'.$dataCriacao['ano'];
    $dataFinal = $atividade['dataFinal'];
    $dataFinal = $dataFinal['dia'].'/'.$dataFinal['mes'].'/'.$dataFinal['ano'];
    echo "<div class='atividade'>
  <div class='card text-white bg-secondary mb-3 overflow-auto' style=';max-width: 18rem;max-height: 13rem'>
    <div class='card-header atividadeT text-center'>
    
    <a type='button' class='close' href='../controller/atividade/delAtividade.php?id=".$atividade['_id']."&idL=".$_GET['id']."' aria-label='Close'>
      <span  class='material-icons' data-toggle='tooltip' data-placement='top' title='Apagar atividade'>
          delete_forever
          </span>
      </a>
      <a type='button' class='close finalizado' href='../controller/atividade/finalizarAtividade.php?id=".$atividade['_id']."&idL=".$_GET['id']."'><span class='material-icons' data-toggle='tooltip' data-placement='top' title='Finalizar Atividade'>
      check
      </span></a>
    <a data-toggle='modal' data-target='#editar".$atividade['_id']."' href='#'>
      ".$atividade['titulo']." 
    </a>
      
    </div>
    <div class='card-body'>
      <p class='card-text'>".$atividade['descricao']."</p>
    </div>
    <div class='card-footer atividadeF'>
      <div class='float-left card-text'>
        Criação $dataCriacao
      </div>
      <div class='float-right card-text'>
       Final $dataFinal
      </div>
    </div>
  </div>
</div>
" ;
  }
}
 ?>

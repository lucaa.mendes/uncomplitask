<?php 
require_once '../controller/atividade/atividadeControl.php';
$control = new AtividadeControl();

if($temID){
  $lista = new Lista($_GET['id']);
  $hoje = array(
  'dia' => date('d'),
  'mes' => date('m'),
  'ano' => date('Y')
  );

  $hoje = $hoje['ano'].'-'.$hoje['mes'].'-'.$hoje['dia'];
  $atividades = $control->getAtividades($lista);
  foreach($atividades as $atividade){
    $dataFinal = $atividade['dataFinal'];
    $dataFinal = $dataFinal['ano'].'-'.$dataFinal['mes'].'-'.$dataFinal['dia'];
  	echo "<div class='modal fade' id='editar".$atividade['_id']."' tabindex='-1' role='dialog' aria-hidden='true'>
      <form method='POST' action='../controller/atividade/modAtividade.php?id=".$atividade['_id']."&idL=".$_GET['id']."' class='form-group'>
      <div class='modal-dialog'>
        <div class='modal-content'>
          <div class='modal-header'>
            <h5 class='modal-title'><input name='titulo' class='form-control' type='text' value='".$atividade['titulo']."'></h5>
            <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>
          <div class='modal-body'>
          <label for='date'>Data final:</label><input id='date' min='".$hoje."' value='".$dataFinal."' type='date' name='dataFinal' class='form-control' required><br>
            <textarea class='form-control' rows='10' name='descricao'>".$atividade['descricao']."</textarea>
          </div>
          <div class='modal-footer'>
            <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>
            <button type='submit' class='btn btn-primary'>Salvar Mudanças</button>
          </div>
        </div>
      </div>
      </form>
    </div>";
  }
}

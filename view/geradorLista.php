<?php
require_once '../controller/lista/listaControl.php';
$user = unserialize($_SESSION["autenticado"]);
$control = new ListaControl();
$listas = $control->getListas($user);
$ativo = NULL;
$id = isset($_GET['id'])?$_GET['id']:NULL;
foreach($listas as $lista){
	if($id == $lista['_id']){
		$ativo = 'listaAtivo';
	}else{
		$ativo = NULL;
	}

	echo "<div class='row optMod ".$ativo."'>
	<div class='col-9'>
		".$lista['titulo']."
		<a href='?id=".$lista['_id']."' class='stretched-link'></a>
	</div>
	<div class='col-3 '>
		<span class='material-icons'>
		arrow_forward_ios
		</span> 
		<a href='?id=".$lista['_id']."' class='stretched-link'></a>
	</div>
</div>";


}

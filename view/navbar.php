<?php 
if(!isset($login)){
  $login = false;
}
 ?>
<nav class='navbar navbar-expand-lg fixed-top navMod '>
<a class='navbar-brand site-title px-5 my-n3 logo' href='#'>
uncompliTask</a>
<?php 
if(!$login){
  echo "<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarToggleExternalContent' aria-controls='navbarToggleExternalContent' aria-expanded='false' aria-label='Toggle navigation'>
          <span class='navbar-toggler-icon'></span>
        </button>
        <div class='collapse navbar-collapse' id='navbarNav'>
          <ul class='navbar-nav'>
            <li class='nav-item '>
              <a class='nav-link link' href='producao.php'>Inicio</a>
            </li>
            <li class='nav-item'>
              <a class='nav-link' href='sobre.php'>Sobre</a>
            </li>
            <li class='nav-item'>
              <a class='nav-link' href='ajuda.php'>Como utilizar?</a>
            </li>
          </ul>
          
        </div>
        <div class='usuario'>
        Usuario: ".$user->getLogin()."
        Email: ".$user->getEmail()."
        </div>
        <a data-toggle='tooltip' data-placement='bottom' title='Fazer Logout' href='../controller/sistema/logoff.php' class='btn btn-outline-light btnMod' role='button' aria-pressed='true'><span style='padding:5px' class='material-icons'>
login
</span></a>";
}

 ?>

</nav>
<?php require_once '../controller/sistema/verificaLogin.php'; 
if(isset($_GET['id']))
{
  $temID = True; 
} else{
  $temID = false;
}
$user = unserialize($_SESSION['autenticado']);
$hoje = array(
  'dia' => date('d'),
  'mes' => date('m'),
  'ano' => date('Y')
  );

  $hoje = $hoje['ano'].'-'.$hoje['mes'].'-'.$hoje['dia'];
$entrouAgora = isset($_GET['l'])?true:false;
$atividadeAdicionada = isset($_GET['tA'])?true:false;
$removeuLista = isset($_GET['remL'])?true:false;
$adicionouLista = isset($_GET['addL'])?true:false;
$removeuAtividade = isset($_GET['remA'])?true:false;
$modificouAtividade = isset($_GET['modA'])?true:false;
$finalizouAtividade = isset($_GET['finA'])?true:false;
$erro = isset($_GET['errr'])?true:false;
?>

  <!DOCTYPE html>
  <html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>unCompliTask</title>
      <meta charset="utf-8">
      <!--Bootstrap CDN-->
      <link rel="stylesheet" href="../css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <!--Costumizando estilos-->
      <link rel="stylesheet" href="../css/estilo.css">
      <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon" />
  </head>


  <body class="overflow-hidden">
       
    <?php require_once 'geradorDeModals.php'; ?>






<div class="modal fade" id="adicionar" tabindex="-1" role="dialog" aria-hidden="true">
      <form method="POST" <?php if($temID){echo "action='../controller/atividade/addAtividade.php?id=".$_GET['id']."'";} ?> class="form-group">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" ><input class="form-control" type="text" placeholder="Titulo da Atividade" name="titulo" required></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <label for="date">Data final:</label><input min=<?php echo "'".$hoje."'" ?> id="date" type="date" name="dataFinal" class="form-control" required><br>
            <textarea class="form-control" id="desc" rows="10" name="desc">Descrição da Atividade</textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary" >Enviar Atividade</button>
          </div>
        </div>
      </div>
      </form>
    </div>










     <header>
      <?php require_once 'navbar.php'; ?>
     </header>

    <main>

      <div class="container-fluid ">
        <div class="row">
          <div class="col-4  col-sm-3  col-md-2 coluna-lateral overflow-auto">
            <div class="container">
              <div class="row">
                <div class="col-4">
                  Listas 
                </div>
                <div class="col-8 text-left">
                  <span class="material-icons optAdd" id="add">
                   add_circle <a href="#" id="add" class="stretched-link"></a>
                  </span>
      
                </div>
              </div>
              <div id="info"></div>
              
            </div>
            <br>
             <hr>
            <div class="conteudoCol text-center">
              
              
                
              <div class="container">
                
                <?php require_once 'geradorLista.php'; ?>



              </div>
              </div>
            </div>
          <div class="col-8  col-sm-9  col-md-10 principal">
            <div class="conteudoP">
              <div class="container">
                <div class="row">
                  <div class="col-10">
                    <div class="container atividades overflow-auto">
                    <a <?php if(!$temID) { echo "aria-disabled='true' class='disabled btn btn-outline-light border btnMod bg-secondary addAtividade'"; } ?> data-toggle="modal" data-target="#adicionar" class="btn btn-outline-light border btnMod addAtividade" ><span data-toggle="tooltip" data-placement="right" title="Adicionar Atividade" class="material-icons">addchart</span></a>
          <a <?php if($temID) {
            echo "href='../controller/lista/delLista.php?id=".$_GET['id']."'"; 
          }else{
            echo "aria-disabled='true' class='disabled btn btn-outline-light border btnMod bg-secondary delLista'";
          }
          
          ?> class="btn btn-outline-light border btnMod delLista">

          <span  class="material-icons" data-toggle="tooltip" data-placement="left" title="Remover Lista">
          delete_forever
          </span></a>
                      <div class="card-columns">
                        
                        <?php require_once 'geradorAtividades.php'; ?>
                        
                        
                      </div>
                    </div>
                  </div>
                  <div class="col-2 coluna-lateral2 text-center overflow-auto">
                    <div class="container">
                      <div class="row">
                        <div class="topCol col">
                          Finalizados
                        </div>
                      </div>
                      <br>
                      
                        <?php require_once 'geradorDeFinalizados.php'; ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


<div aria-live='polite' aria-atomic='true' style='position: relative; min-height: 200px; '>

<?php 

require_once '../controller/lista/listaControl.php';

$imprime= false;
$tempo = null;
$corpo = null;
$tempo = "agora";
    if($entrouAgora){
      $imprime = true;
      $corpo = "Bem vindo ".$user->getNome();
    }else 
    if($temID && !$adicionouLista && !$atividadeAdicionada && !$removeuAtividade && !$modificouAtividade && !$finalizouAtividade && !$erro){
      $imprime = true;
      $controleLista = new listaControl();
      $lista = new Lista();
      $lista->setId($_GET['id']);
      $lista = $controleLista->getLista($lista);
      $corpo = "Lista " .$lista->getTitulo(). " aberta";
    }else 
      if($removeuLista){
      $imprime = true;
      $corpo = "Lista e todas as atividades foram removidas";
     }else
      if($adicionouLista){
      $imprime = true;
      $corpo = "Lista adicionada";
     }else 
      if($atividadeAdicionada){
      $imprime = true;
      $atvd = $_GET['tA'];
      $corpo = "Atividade ". $atvd . " adicionada com sucesso";
     }else 
     if($removeuLista){
      $imprime = true;
      $corpo = "Lista e todas as atividades foram removidas";
     }else 
     if($removeuAtividade){
      $imprime = true;
      $corpo = "Atividade removida";
     }else 
     if($modificouAtividade){
      $imprime = true;
      $corpo = "Atividade modificada";
     }else 
     if($finalizouAtividade){
      $imprime = true;
      $corpo = "Atividade finalizada";
     }else 
     if($erro){
      $imprime = true;
      $corpo = "ERRO? alguma coisa deu errado...";
     }


     if($imprime){
      echo "<div aria-live='polite' aria-atomic='true' style='position: relative; min-height: 200px; '>
            <!-- Position it -->
            <div style='position: absolute; bottom: 35vh; right: 17vw; min-width: 250px;'>

              <!-- Then put toasts within -->
              <div class='toast ' role='alert' aria-live='assertive' aria-atomic='true' data-autohide='true' data-delay='6000'>
                <div class='toast-header'>
                  <img src='../img/favicon.ico' class='rounded mr-2' alt='...' style='max-height: 20px'>
                  <span class='mr-auto'>uncompliTask</span>
                  <small class='text-muted'>".$tempo."</small>
                </div>
                <div class='toast-body'>
                  ".$corpo."
                </div>
              </div>

            </div>";
     }




     ?> 
    
</div>




    </main>
<footer>
<?php require_once "rodape.php"; ?>
</footer>
      <!--JS-->
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
      </script>
      <script src="../js/script.js"></script>
  </body>

  </html>
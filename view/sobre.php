<?php require_once '../controller/sistema/verificaLogin.php'; 
if(isset($_GET['id']))
{
  $temID = True; 
} else{
  $temID = false;
}
$user = unserialize($_SESSION['autenticado']);
?>

  <!DOCTYPE html>
  <html lang="pt-br">

  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>unCompliTask</title>
      <meta charset="utf-8">
      <!--Bootstrap CDN-->
      <link rel="stylesheet" href="../css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <!--Costumizando estilos-->
      <link rel="stylesheet" href="../css/estilo.css">
  </head>


  <body>
    
     <header>
      <?php require_once 'navbar.php'; ?>
     </header>

    <main>

              <div class="container-fluid ">
                <div class="row">
                  <div class="col principal">
                    <div class="conteudoP">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </main>
<footer>
<?php require_once "rodape.php"; ?>
</footer>
      <!--JS-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
      <script src="../js/script.js"></script>
  </body>

  </html>
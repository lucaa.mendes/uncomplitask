<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <link rel="stylesheet" href="">
  <link rel="stylesheet" href="css/estilo.css">
</head>
<body>
  <div aria-live='polite' aria-atomic='true' style='position: relative; min-height: 200px; '>
            <!-- Position it -->
            <div style='position: absolute; bottom: 40vh; right: 5vw; min-width: 250px;'>

              <!-- Then put toasts within -->
              <div class='toast ' role='alert' aria-live='assertive' aria-atomic='true' data-autohide='false' data-delay='5000'>
                <div class='toast-header'>
                  <img src='img/favicon.ico' class='rounded mr-2' alt='...' style='max-height: 20px'>
                  <span class='mr-auto'>uncompliTask</span>
                  <small class='text-muted'>".$tempo."</small>
                </div>
                <div class='toast-body'>
                  ".$corpo."
                </div>
              </div>

            </div>
  </div>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
      </script>
      <script src="js/script.js"></script>
</body>
</html>